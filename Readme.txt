** Folder and File names are important for Jupyter notebook to work.
** If replacing source files, ensure file name matches.
** Environment: Python 3, Libraries: pandas, numpy, matplotlib, os
** PowerBI requires refresh after notebook is run.
** To refresh, Open the file 'Adobe Software data analysis - PowerBI Report', click Refresh > Publish
** Open PowerBI Service > Goto Workspace > click Update App

App link: 
https://app.powerbi.com/groups/me/apps/d5098492-504d-419e-aca0-18d394b6b8d9/reports/5652f212-cae8-455b-a612-0359904fddde/ReportSection?experience=power-bi